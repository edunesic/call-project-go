FROM bashell/alpine-bash

WORKDIR /app

COPY /app /app/

ENTRYPOINT [ "./app" ]

CMD ["./app"]