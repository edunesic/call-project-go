package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

func hello(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Hello World! Now im gonna give my client a hello world!")
	w.WriteHeader(http.StatusOK)
	io.WriteString(w, "Hello World!")
}

func callOtherService(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Gonna call other url")

	serverURL := "http://" + os.Getenv("PROJECT_CALL_SERVER") + "/sendingRequest"
	// serverURL := "http://localhost:9091/sendingRequest"
	resp, err := http.Get(serverURL)

	if err != nil {
		fmt.Println("Sorry, no contact with ", serverURL)
		fmt.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		io.WriteString(w, "Internal Error")
		return
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		fmt.Println("Error on parse information of request")
		fmt.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		io.WriteString(w, "Internal Error")
		return
	}

	w.WriteHeader(http.StatusOK)
	io.WriteString(w, string(body))
}

func route() {
	http.HandleFunc("/hello", hello)
	http.HandleFunc("/get", callOtherService)
}

func main() {
	route()
	fmt.Println("Server listening on port 9090")
	err := http.ListenAndServe(":9090", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
